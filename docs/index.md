# SAS G-CLI Tools Documentation

## Getting Started

{{ initialize() }}

### About

I often get questions about how to get started with Continuous Integration and Continuous Delivery (CI/CD) in LabVIEW. Many developers see the value in it, but are unsure how to get started. The first step is usually to be able to use the CLI to automate various development tasks. That is what these tools are for. 

SAS G-CLI Tools is a set of Open Source [G-CLI](https://github.com/JamesMc86/G-CLI) tools for common LabVIEW Tasks. Each is provided in its own VIPM package, so you can install just the ones you want. Each G-CLI tool is also Open Source and hosted in it's own GitLab repository, so you can fork them and modify them all you want to match your specific workflow.

These tools are useful for automating your testing and build process in LabVIEW. Using these tools, you can write a simple bash script to run your unit tests and then if they pass build your executable and package it up. This helps you create a robust and repeatable process. These essential tools are the building blocks of a CI/CD pipeline.

### Why?

CI/CD is the best investment you can make in risk management for your project. It prevents the 2 major sources of software defects: building the **THING** wrong and building the **WRONG** thing. Regularly running unit tests via CI helps to ensure that the software works as you intend and that you are building the **THING** right. Continuously delivering software to you users using CD ensures that you can get quick feedback from your users to make sure you are all on the same page and building the **RIGHT** thing.

Setting up CI/CD can be daunting. There are a lot of moving parts. There's also not a lot of good documentation out there. This project hopes to change that.

### New to CI?

Starting out with CI can be overwhelming.
If you are completely new to it all, here is how I recommend getting started.

1. Checkout our [introductory videos](/#introductory-videos). It will give you a good overview of what is possible and what is involved.
1. Map out your current process. Think about what happens after you make a change to your software. What steps do you take in order to verify that change works and didn't break anything. Then how do you build your application and distribute it. Write it all down.
1. Look through the list of G-CLI tools here and connect the steps in your process to the G-CLI tools. Write a bash script that calls each of the steps in order. The goal is a single batch script that developers run locally that will run your tests and build your program and installer.
1. Once you have that script, then head to the [Setting up a GitLab Runner](/#setting-up-a-gitlab-runner) section. It will walk you through how to setup a GitLab Runner and get it to execute your script.
1. Also checkout one of our [Sample Projects](/#sample-projects). It will provide some inspiration and/or a useful starting point.
1. Collect feedback and improve. Once you have a pipeline in place, then it easy to tweak existing steps and add now ones.

At some point after step 3 it is worth checking out the [sample projects](/#sample-projects). They may do exactly what need. They will at least show you what is possible and point you in the right direction of putting all the pieces together.

### Already using some form of CI?

Head to our [sample projects](/#sample-projects) to see these tools in use. The examples are all GitLab repositories that you can fork and experiment with. You'll also want to check out the section on [Setting Up Dynamic Runners](#/setting-up-dynamic-runners)

### Introductory Videos

These G-CLI tools are one of the first steps in setting up a CI/CD Pipeline. Here are some introductory videos to GitLab CI and CI/CD in general. None of these are specifically about SAS-GCLI-Tools, but they provide some good and necessary background information. These links are here to help get you up and running with the rest of that CI/CD pipeline setup.

* [CI Intro/Demo Webinar](https://youtu.be/SzINlGE35VE) - A webinar on CI/CD using GitLab 
* [NI Connect 2023 Panel on CI](https://youtu.be/ctX1GNCO5Wc) - A panel discussion about CI at NI Connect, featuring Sam Taggart, Jim Kring, Joerg Hampel, and Chris Roebuck
* [GDevCon #2 Panel Discussion on CI](https://www.youtube.com/watch?v=M3ZJl1sB2SQ) - Some good general thoughts and an overview of CI along with some use cases.
* [LAF Presentation on Getting Quicker Feedback - Getting Started With CI](https://www.youtube.com/watch?v=fxouNp9n41g) - A good overview of CI.
* [GLA 2022 - What is this CD thing?](https://youtu.be/DvdCq5Svohc) - A demo of a couple options for extending your CI pipeline to actually deploying code.
* [Setting Up a GitLab Runner](https://www.youtube.com/watch?v=mwSo7ozKx2k) - A walkthrough of setting up a GitLab Runner

Here are some other alternative options worth exploring:

* [Exploring BLT](https://youtu.be/b0prZoL1kdo) - A demonstration of BLT, which is an alternative way to do a lot of CI type things.
* [Getting Started with GitHub Actions](https://www.youtube.com/watch?v=rVIrfbSbLCw)  - A handson presentation that walks you through setting up a GitHub agent (similar to a GitLab Runner)
* [Nike Naredi on Azure DevOps](https://www.youtube.com/watch?v=AU-06R5b85Y) - This presentation by Niko is basically what I outline here for GitLab except it is for Azure DevOps.

### Installation

Each of these G-CLI Tools is available on VIPM. That is the easiest way to install them. 

Here are the links to download the latest packages from VIPM.
{% for tool in tools() %}
* [![stars badge]( {{ tool["vipm_badge_stars"] }})]( {{ tool['vipm_url'] }})  [![installs badge]( {{ tool["vipm_badge_installs"] }})]( {{ tool["vipm_url"] }} ) [{{ tool['official_name'] }}]({{ tool['vipm_url'] }}) 
{% endfor %}

### Liscensing

All of the tools are licensed under the [MIT license](https://opensource.org/license/mit/t p). A license file is included in each VIPM installation package.

### Verifying G-CLI Works

You need to make sure that G-CLI is in your PATH. Instructions for that differ based on what shell you are using.

To test G-CLI you can simply do the following:

```bash
g-cli echo -- "hello world"
```

### Supported Versions

- LabVIEW -These tools are packaged with VIPM using LabVIEW 2020, so they should work with any LabVIEW bitness, version 2020 or later. 
- OS - These tools are Windows only at the moment, although the latest G-CLI apparently also works on Linux, so perhaps that may work. If you try it and find out, please let me know. I'm still on Windows 10, but there is no reason they shouldn't work with Windows 11.
- G-CLI - 2.4.0.4 or later.


## Available Commands

{% for tool in tools() %}
* [{{ tool['cmd'] }}](/#{{ tool['cmd'] }}) - {{ tool['short_description'] }}
{% endfor %}


{% for tool in tools() %}

### {{ tool['cmd'] }} 
[![image]( {{ tool["gitlab_release_badge"] }} )]( {{ tool["gitlab_release_url"] }} ) [![stars badge]( {{ tool["vipm_badge_stars"] }})]( {{ tool['vipm_url'] }})  [![installs badge]( {{ tool["vipm_badge_installs"] }})]( {{ tool["vipm_url"] }} )

#### Description 

{{ tool['description'] }}

#### Options

{{ tool['options'] }}

#### Example

{{ tool['example'] }}

#### Repository

For more information and to see the source code, check out the repository here:
[{{ tool['base_url'] }}]({{ tool['base_url'] }})

{% endfor %}

### Third Party CLI Tools for other common commands

These are tools I didn't bother to implement G-CLI commands for, since they are already provided. You may find these useful. I'm not going to document them here, since the developers have already done that.

- [Caraya](https://www.vipm.io/package/lvos_lib_caraya_cli_extension/)
- [AntiDoc](https://www.vipm.io/package/wovalab_lib_antidoc_cli/)


## Setting Up  Static Runners 

In order to fully take advantage of CI, at some point you are going to need to set up a runner. Static Runners are an easy starting point. This is a machine (VM or physical) that is constantly running. Your jobs run directly on this machine. This makes for easy setup and troubleshooting. 

For CI, I typically use GitLab, so these instructions are tailored to GitLab (I'll also include some info for GitHub users). On my [blog](https://blog.sasworkshops.com) I have a few posts about setting up GitLab Runners. It's spread over a few articles and a little disjointed. I'll attempt summarize it all in one place here. If you prefer video I have a video of me [walking Casey through the process](https://www.youtube.com/watch?v=mwSo7ozKx2k). It was made before this tutorial so it might not match up exactly.

Here is a diagram showing the basic setup.

![CI Diagram](assets/simple-ci-diagram.png)

### How Many Runners Do I Need?

This is a common question. It is possible to have a single runner with multiple versions of LabVIEW installed. In the past multiple versions of LabVIEW have not always played well together so I recommend against this. I recommend one runner per version of LabVIEW or one per project if your projects have 3rd party drivers or other unique requirements. This keeps things better isolated. This can result in a lot of machines, so Virtual Machines are your friends here or Docker if you are brave, but that is a whole other can of worms. You can save yourself a little trouble by having a common Win10 or W11 machine that you copy as a starting point. You probably don't want to clone it - that can cause some problems. Better to just copy the VM Disk and create a new VM from it.

### Assumptions

I'm making a few assumptions here. I am assuming you are using Windows (it should be similar for Win10 or Win11). I'm also going to be setting this up to use Git Bash instead of Powershell. I detest Microsoft and I don't know Powershell, but I do know Bash, so that is what I am going to use. If you want to use Powershell, the initial setup will be a little easier (because that is the default for GitLab shell runners on Windows). However you may have to tweak some of the yaml files and scripts. I am also assuming that you are using VIPM pro or the Community Edition. You need one of those in order for the vipc applier tool to work. 

### Starting Point

As a runner you will generally want a seperate machine from your dev machine. This could be a spare laptop or desktop, a VM running on your own desktop or server or a VPS running on something like Amazon, Vultr, or Digital Ocean. You'll need just enough horsepower to load your project and run a build. It will need access out to the internet to be able to ping gitlab.com (or if you are self-hosting, then whatever your url is for your GitLab server). You should consider keeping it on a seperate network isolated from your main network, because running a build basically opens up a reverse shell.

**NOTE**: If you are just starting out and dabbling with CI, an easy solution is to install GitLab Runner on your Development machine. It is much less than ideal, but lowers the barrier to entry and allows you to easily see what is going on. In that case you can probably skip the section on setting the script to run on startup and setting up autologon.

### Access to Runner Machine

You will want to be able to logon to the runner and view the GUI for setup/installation and troubleshooting. For a physical machine that should be easy. RDP works best for remote machines, but VNC, MSP360 or Team Viewer will also work. Don't expose RDP or VNC to the internet. It is not secure. Run it through some encrypted tunnel like ssh, a vpn, wireguard, Nebula, etc. 

### Initial Software Installation

I'm assuming you are using Windows, so you will want to install the appropriate Windows Version and turn on Remote Desktop (if that is what you are using for access) You will want to install Git For Windows along with Git Bash. The default options should be fine. You will want to install NIPM and use that to install the proper version of LabVIEW, VIPM, DAQmx and any other drivers you need. You'll also want to install some basic VIPM packages such as G-CLI and the VIPM API. You'll also want to install whatever of the SAS-GCLI tools you want to use. If you have the Pro or Community edition of VIPM, then you can use the vipc applier tool to install packages as part of your CI pipeline. If you aren't doing that, then you should probably install the needed packages now. If you are using Python to run any build scripts or utilities, now is a good time to install that and any needed Python packages as well.

You really don't want to install a lot of extras. You are not going to be using this machine for anything other than running builds. Running a build basically opens up a reverse shell, so you don't want anything sensitive on there other than your code. 

### Initial Verification

You should manually open VIPM and LabVIEW and make sure no licensing dialogs pop up and take care of them if they do. You should verify that VIPM can connect to your version of LabVIEW by opening VIPM and going to the settings dialog box. You should also verify G-CLI is installed correctly and can connect to LabVIEW by typing `g-cli echo -- "Hello World"` into Git Bash and verifying that it returns "Hello World". If it doesn't, check your $PATH and make sure it contains the G-CLI directory. You may need to add an export line to your .bashrc file.

### Setting Up GitLab Runner

#### Installing GitLab Runner

The instructions for installing the gitlab-runner program [provided by GitLab](https://docs.gitlab.com/runner/install/windows.html) are pretty good. Make sure you select shell as your executor when it asks. It will also ask you to give it a name and assign a tag to it. You can change these later through the web interface. Generally you'll want to tag a runner either with the name of a specifc project or based on the software versions installed. The tags are used to determine what jobs GitLab can run on this particular runner. You should just follow the instructions up until the point where it tells you to install it as a service. If you want to be able to actively debug and watch what is going on, you don't want to do this. So stop after step 5 and don't proceed to step 6.

[![gitlab-runner installation instructions](assets/runner_install.png)](https://docs.gitlab.com/runner/install/windows.html)

#### Setting up GitLab Runner to use Git Bash Instead of PowerShell

**NOTE:** This section describes how to set up GitLab Runner to use Git Bash. If you want to use PowerShell you can skip this.

First, we need to help GitLab Runner find GitBash. Add a bash.cmd file into your user directory (the user that runs the GitLab Runner) with the following contents:

`@"C:\Program Files\Git\usr\bin\bash.exe" -l`

Next, we have to make sure that our starting paths are set up correctly with Linux-style formatting. In your config.toml file for your GitLab runner (it’s in the directory where you installed GitLab Runner), change the shell to bash and add Linux style paths for the builds_dir and cache_dir. That’s it. See the example below.

```bash
[[runners]]
  name = "windows"
  url = "https://your.server.name"
  token = "YOUR_SECRET_TOKEN"
  executor = "shell"
  shell = "bash"
  builds_dir="/c/gitlab-runner/builds/"
  cache_dir="/c/gitlab-runner/cache/"
```
**NOTE on Paths**: We are using linux paths here, but when passing paths as parameters to the g-cli tools (ie the parameters after the -- ) you will need to use Windows Paths.

#### Running as a User

Create a batch file with the following line in it.

`"C:\Program Files\Git\bin\sh.exe" --login -i -c "cd /c/Gitlab-Runner && ./gitlab-runner.exe run"`

Save it somewhere convenient, like the Desktop and give it a meaningful name like "Start GitLab Runner.bat" Running this batch file will open a console window as seen below. Once a job starts, then you’ll see all the windows that it opens. You’ll see the LabVIEW splash screen and any VIs it opens. So if it hangs on some dialog, you will know about it.

### Set to run at Startup

In order to run the script at startup (when the current user logs in), drop a link to into the Windows Startup Folder. You can find the Windows startup folder by hitting the windows key + R and typing `shell:startup` into the resulting dialog box.

### Setting User to Autologon

The last thing I typically do is set the user to autologon, otherwise every time the machine boots up, you have to login to the machine before gitlab-runner can start. The way to do that in windows is to use netplwiz. Hit windows key + R and type in `netplwiz`. It will open a User Accounts window. You should see a checkbox at the top that says "Users must enter a user name and password to use this computer." Simply uncheck it.

![netplwiz](assets/netplwiz.png)

**NOTE**: You may not see that checkbox. If you don't you can try adding a registry key. Copy the code below. Open a command prompt as administrator. Paste and run the code. Reboot. The box should show up now. If not, there are some other potential fixes, just google it.

`reg ADD “HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\PasswordLess\Device” /v DevicePasswordLessBuildVersion /t REG_DWORD /d 0 /f`

At this point I usually reboot the runner and just sit and watch to make sure when it boots up it doesn't hang waiting for a Windows password and that the batch script runs on startup.

### Verifying in GitLab

#### Verifying the Connection

If you log into your project and go to the CI/CD settings and the runner is up and running (ie you have ran the batch file we created), you should see it in the list of assigned runners.

![CICD Runners Settings Page](assets/settings_cicd_runners.png)]

#### Assigning Runners and changing tags

Once you have a runner up and connected to your gitlab server, it is easy to make changes to it through the web interface. You can change the name and the tags. You can also enable it for different projects (by default they are locked to a specific project, so you have to unlock it first). You can do all this by clicking on the edit button next to the runner to open the runner configuration editor.

![Runner Configuration](assets/individual_runner_settings.png)

### Success!

Once you have done all the above steps, your runner should be good to go.

## Setting Up Dynamic Runners

This section is based on [this blog article](https://blog.sasworkshops.com/spawning-vms-like-docker-containers/). It's about how to use the GitLab Custom Executor to spawn KVM VMs on demand and run jobs on them. It should work for any VM provider. The code is in [this repository](https://gitlab.com/sas-gcli-tools/kvm-executor/). You'll see that it uses some shell scripts to launch the VMs and then connects to the VMs over SSH to run the commands. There is no reason the shell scripts couldn't me modified to use another VM provider or some cloud provider.   

Here is a diagram of the overall architecture that may be helpful.

![KVM Executor](assets/KVM-Executor.png)

**NOTE** This is an advanced topic so I assume you have some idea of what you are doing. I make some assumptions and leave some details out.

### Disadvantages

- The main disadvantage of this approach is it is slightly more complex
- As-is the scripts only work with GitLab and KVM on Linux. It should be portable to other platforms. For GitHub, I looked into it and there is a webhook that fires when a new job is queued. So that could be used to execute a shell script to start a VM and register it with GitHub as an ephemeral runner. If you get that to work, let me know.

### Advantages

This approach offers several advantages.

- Clean VM each time - avoid residue being left over from one job to the next. Ensures you have included all dependencies.
- Scaling and Running in Parallel - This allows you to run multiple different (or same) VMs in parallel.
- Ease of setting up New Base VMs
- Better Resource Utilization - If you have several projects, you only have 1 runner listening.

### Setting Up The Host

I use KVM on Linux. Google how to set that up. Then I clone [these scripts](https://gitlab.com/sas-gcli-tools/kvm-executor) and place them somewhere accessible by the user that runs KVM. You also need a directory full of Base VMs and a directory for clones. Putting all clones in one directory makes housekeeping easier if something get's orphaned. Getting the permissions right can be a challenge. I suggest starting with a project with a really simple hello world and getting that to work first. Also you'll need to setup an SSH key to ssh into the VMs without a password - so don't make the key password protected!

### Setting Up The Base VM Images

This is for Windows machines. I generally install Git For Windows and NIPM. Then I set up SSH and everything else. Then I clone that VM and install LabVIEW and VIPM. That gives me 2 base VMs. One with just Git and NIPM so I can test installs and one with LabVIEW, VIPM, etc for running my builds.

#### Windows SSH

You need to setup SSH server on your Windows VM. See [this blog article](https://blog.sasworkshops.com/ssh-server-on-windows-10/)

#### Necessary Packages For BootStrapping

You'll need the G-CLI, VIPM API and the VIPC Applier packages as a minimum. With that you can use the VIPC Applier to install anything else you need. Remember this happens on every job. If it starts taking too long, you can create a seperate image for a particular project with the vipc preinstalled. I usually keep the VIPC step in that case anyway, just incase I change something like add or upgrade a package.

#### GitLab Runner

You only technically need GitLab Runner on the KVM host. However if you want to upload artifacts then you need gitlab-runner on the base VM and in the path. You don't register it, but it does have to be in the path.

### Setting Up The gitlab-ci.yml File

You have 2 things that you need to add to your yml file in order to run a job this way. First you need a tag to tell GitLab which runner to dispatch the job to. I use "kvm" for this. You also need a $BASE_VM variable to tell the runner which VM to spin up and run the job on.

### Parallel Jobs

You can enable parallel jobs by editing the runner config on the KVM host. Just change the concurrent setting.

## Sample Projects 

Once you have a runner setup, the next step is to test it out. An easy way to do that is to use one of our sample projects. You can simply fork the project. Then make sure that the tag in the CI matches your runner and your runner is assigned to the project and it should be good to go. Make a change, commit, and push and it should kick off a pipeline. If you aren't using VIPM pro, you may need to manually apply the VIPC to the runner, but other than that it should work.

{% for sample_project in sample_projects() %}
* [{{sample_project['title']}}]({{sample_project['base_url']}})
{% endfor %}

## Why Bash scripts?

You'll notice in the sample projects I put all the commands in Bash scripts. Why is that? It gives us 2 things.

1. It allows us to run the commands locally (ie `./build.sh`)
1. It makes your CI more portable. Almost any CI system can run bash scripts. The difference between GitHub and GitLab is just in how we tell the system which script to run and when. 

### New to Bash?

I chose Bash because I am familiar with it (more than PowerShell) and because it is cross-platform. If you are getting started, I recommend purchasing [this cheatsheet](https://wizardzines.com/zines/bite-size-bash/). The biggest takeaway is to make sure you add `set -euo pipefail` at the top of your bash scripts so the error handling and exit codes work correctly.

### A Note On Variables

In the GitLab examples, you'll notice this line:

``` bash
. <(sed -nr '/variables:/,$ s/  ([A-Z_]+): (.*)/\1=\2/ p' vars.yml)
```

What this line does is that it reads in the vars.yml file and takes the variables section and sets the variables there as environment variables. This is so we can define the variables once and use them when running locally or on the CI server (this file is included in the .gitlab-ci.yml file). The sed command is kind of black magic but it works. I got it here: [https://unix.stackexchange.com/questions/539009/export-environment-variables-parsed-from-yaml-text-file](https://unix.stackexchange.com/questions/539009/export-environment-variables-parsed-from-yaml-text-file) 

## Pylavi

[Pylavi](https://github.com/marcpage/pylavi) is a tool written in Python that parses LabVIEW files as binary files without actually loading them. It is very useful for CI because it is superfast and only requires Python to run, so it's easy to use - no need to setup a runner or anything, tell GitLab CI to use a Python Docker container and `pip install` it. It can check LV Versions of files. It can check for separate compile code and automatic error handling. It can also check for breakpoints and suspend on call and a variety of other things.

Here is a simple example of using pylavi:

``` bash
pylavi:
  stage: inspect
  image: python:3
  script:
    - pip install pylavi
    - vi_validate
        --path source 
        --no-code 
        --breakpoints 
        --no-suspend-on-run 
        --autoerror
```

Key points are:
- You first `pip install pylavi`
- the command you then use is `vi-validate` 
- the `--path` option tells it which folder to look in  
- `--no-code` means fail if compiled code is not separated
- `--breakpoints` means fail if any breakpoints found
- `--no-suspend-on-run` means fail if suspend on run found
- `--autoerror` means fail if autoerror handling is enabled

## Need Help?

I have lots of faith in you the reader. I also have some confidence that I've laid out all the pieces here with enough detail. You are probably capable enough to figure this out all on your own.

**And Yet...**

- CI/CD can be overwhelming.
- There are lots of moving parts and different (and perhaps unfamiliar) technologies involved.
- There are lots of opportunities for things to go wrong.
- Setting it up takes a lot of effort.

So if you are stuck on a particular step, overwhelmed and don't know where to start, or you just want to press the easy button, I do offer consulting services and I'd be happy to help you.

Reach out via [https://sasworkshops.com/contact](https://sasworkshops.com/contact) and someone will reach out to set up a call to discuss working together.

### Workshops

In addition to consulting, I also offer a [CI/CD workshop](https://www.sasworkshops.com/ci-cd-workshop) along with some other related workshops like [Unit Testing](https://www.sasworkshops.com/Unit-Testing-Workshop). I also have a [Using Git Effectively](https://www.sasworkshops.com/using-git-effectively-course-preview) pre-recorded online course, if you need help with that.

## Was This Helpful?

If you found this website or any of the tools helpful, we would very much appreciate it if you were to tell all your friends, especially the ones with money who might consider hiring us. If they seem interested, have them fill out the [contact form on our website](https://sasworkshops.com/contact) and we'll take it from there.