# SAS GCLI  Tools Documentation
[TOC]

## About

This is the documentation generator for the documentation site for the SAS G-CLI tools. These are G-CLI Tools for executing common tasks needed for CI/CD


This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml). It uses the static site generator MkDocs to generate the site. 

Read more about [MKDocs].


## Building locally

To work locally with this project, you'll have to follow the steps below:

1. clone this project
1. `pip install -r requirements.txt`
1. Preview your project: `mkdocs serve`.
   Your site can be accessed under `localhost:8000`. 
   If you run into errors try `export PYTHONPATH=$(pwd)` first.
1. Add content by editing `doc/index.md`
1. Preview should live update
1. Push changes on main branch to deploy via CI



## GitLab Group Pages

This site is deployed to GitLab Pages.

url: https://sas-gcli-tools.gitlab.io/sas-gcli-tools-gitlab-io

[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: http://www.mkdocs.org

---

Orignally forked from https://gitlab.com/morph027/mkdocs
