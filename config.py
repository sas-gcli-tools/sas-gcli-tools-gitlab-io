TOOLS = [
    {
        "cmd": "clearlvcache",
        "official_name": "Clear LabVIEW Cache For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/clearlvcache",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_clearlvcache_for_g_cli",
    },
    {
        "cmd": "vipc",
        "official_name": "VIPC Applier For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/vipc",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_vipc_applier_for_g_cli",
    },
    {
        "cmd": "lvbuildspec",
        "official_name": "LVBuildSpec For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/lvbuildspec",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_lvbuildspec_for_g_cli",
    },
    {
        "cmd": "vitester",
        "official_name": "VITester For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/vitester",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_vitester_for_g_cli",
    },
    {
        "cmd": "lunit",
        "official_name": "LUnit For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/lunit",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_lunit_for_g_cli",
    },
    {
        "cmd": "vipb",
        "official_name": "VIPB Builder For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/vipb",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_vipb_builder_for_g_cli",
    },
    {
        "cmd": "vip",
        "official_name": "VIP Installer For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/vip",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_vip_installer_for_g_cli",
    },
    {
        "cmd": "switcheroo",
        "official_name": "Switcheroo For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/switcheroo",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_switcheroo_for_g_cli",
    },
]

SAMPLE_PROJECTS = [
    {
        "title": "GitLab: DQMH CML CI Template Project",
        "base_url": "https://gitlab.com/sas-gcli-tools/dqmh-cml-ci-example",
    },
    {
        "title": "GitLab: VIPM Package CI Example",
        "base_url": "https://gitlab.com/sas-gcli-tools/vipm-package-ci-example",
    },
    {
        "title": "GitLab: DQMH CML Auto Updating Example",
        "base_url": "https://gitlab.com/sas-blog/GLA22-SAS-CD-Demo",
    },
    {
        "title": "GitHub: A very simple example of using GitHub Actions",
        "base_url": "https://github.com/stagg54/test-lv-ci/",
    },
]
